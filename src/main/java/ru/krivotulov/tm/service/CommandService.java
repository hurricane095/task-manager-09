package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.ICommandRepository;
import ru.krivotulov.tm.api.ICommandService;
import ru.krivotulov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
