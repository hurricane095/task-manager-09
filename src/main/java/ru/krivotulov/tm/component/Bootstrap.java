package ru.krivotulov.tm.component;

import ru.krivotulov.tm.api.ICommandController;
import ru.krivotulov.tm.api.ICommandRepository;
import ru.krivotulov.tm.api.ICommandService;
import ru.krivotulov.tm.constant.ArgumentConst;
import ru.krivotulov.tm.constant.TerminalConst;
import ru.krivotulov.tm.controller.CommandController;
import ru.krivotulov.tm.repository.CommandRepository;
import ru.krivotulov.tm.service.CommandService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        commandController.displayWelcome();
        process();
    }

    private void process() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {
                while (true) {
                    System.out.println("ENTER COMMAND:");
                    runCommand(bufferedReader.readLine());
                }
            } finally {
                bufferedReader.close();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case (TerminalConst.COMMANDS):
                commandController.displayCommands();
                break;
            case (TerminalConst.ARGUMENTS):
                commandController.displayArguments();
                break;
            case (TerminalConst.HELP):
                commandController.displayHelp();
                break;
            case (TerminalConst.VERSION):
                commandController.displayVersion();
                break;
            case (TerminalConst.ABOUT):
                commandController.displayAbout();
                break;
            case (TerminalConst.INFO):
                commandController.displaySystemInfo();
                break;
            case (TerminalConst.EXIT):
                close();
                break;
            default:
                commandController.displayError(command);
                break;
        }
    }

    private boolean runArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String param = args[0];
        runArgument(param);
        return true;
    }

    private void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case (ArgumentConst.COMMANDS):
                commandController.displayCommands();
                break;
            case (ArgumentConst.ARGUMENTS):
                commandController.displayArguments();
                break;
            case (ArgumentConst.HELP):
                commandController.displayHelp();
                break;
            case (ArgumentConst.VERSION):
                commandController.displayVersion();
                break;
            case (ArgumentConst.ABOUT):
                commandController.displayAbout();
                break;
            case (ArgumentConst.INFO):
                commandController.displaySystemInfo();
                break;
            default:
                commandController.displayError(arg);
                break;
        }
    }

    private static void close() {
        System.exit(0);
    }

}
