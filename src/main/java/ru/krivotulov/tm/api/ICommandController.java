package ru.krivotulov.tm.api;

public interface ICommandController {

    void displayWelcome();

    void displayArguments();

    void displayCommands();

    void displayHelp();

    void displaySystemInfo();

    void displayVersion();

    void displayAbout();

    void displayError(String arg);

}
