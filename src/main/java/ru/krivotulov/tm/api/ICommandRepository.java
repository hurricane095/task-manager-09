package ru.krivotulov.tm.api;

import ru.krivotulov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
